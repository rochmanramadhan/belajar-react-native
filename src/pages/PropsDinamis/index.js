import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';

// membuat Component yang dinamis dengan menggunakan Props
const Story = (props) => {
  return (
    <View style={{alignItems: 'center', marginRight: 20}}>
      <Image
        source={{
          uri: props.image,
        }}
        style={{width: 80, height: 80, borderRadius: 40}}
      />
      <Text style={{maxWidth: 50, textAlign: 'center'}}>{props.title}</Text>
    </View>
  );
};

const PropsDinamis = () => {
  return (
    <View>
      <Text>Materi Component Dinamis dengan Props</Text>
      <ScrollView horizontal>
        <View style={{flexDirection: 'row'}}>
          <Story
            title="Youtube Channel"
            image="https://placeimg.com/1000/400/any"
          />
          <Story
            title="Kelas Online"
            image="https://belajarreactjs.com/wp-content/uploads/2019/11/state-dan-props-768x403.png"
          />
          <Story
            title="Kabayan Coding"
            image="https://i.ytimg.com/vi/f8Uiz7urTPw/hqdefault.jpg?sqp=-oaymwEYCKgBEF5IVfKriqkDCwgBFQAAiEIYAXAB&rs=AOn4CLD36_I3f8VIeQgjQBwxdEThGPAzsQ"
          />
          <Story
            title="Shoot"
            image="https://i.ytimg.com/vi/gWqgPJ_j2LI/hqdefault.jpg?sqp=-oaymwEYCKgBEF5IVfKriqkDCwgBFQAAiEIYAXAB&rs=AOn4CLD_BKYXlcs_4lTojUdd9gf3XZHlrg"
          />
          <Story
            title="Food"
            image="https://i.ytimg.com/vi/-5tVOhlxrMY/hqdefault.jpg?sqp=-oaymwEYCKgBEF5IVfKriqkDCwgBFQAAiEIYAXAB&rs=AOn4CLCxq9wdFelK9iIBMsPwON56XOpc6A"
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default PropsDinamis;
