import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const BasicJavascript = () => {
  // Tipe Data

  // primitive
  const nama = 'Rochman Ramadhani'; // String
  let usia = 24; // Number
  const apakahLakiLaki = true; // Boolean

  // complex
  const hewanPeliharaan = {
    nama: 'Miaw',
    jenis: 'Kucing',
    usia: 2,
    apakahHewanLokal: true,
    warna: 'kuning',
    orangTua: {
      jantan: 'Kaisar',
      betina: 'Kuin',
    },
  }; // Object

  const sapaOrang = (name, age) => {
    return console.log(`Hello ${name} usia Anda ${age}`);
  }; // Function

  sapaOrang('Oman', 18);

  const namaOrang = ['Rochman', 'Ramadhani', 'Chiefto', 'Irawan']; // object - array

  typeof namaOrang; // object

  // Conditional Statement
  if (hewanPeliharaan.nama === 'Miaw') {
    console.log('Halo Miaw');
  } else {
    console.log('Hewan sapa ni?');
  }

  const sapaHwan = (objectHewan) => {
    // let hasilSapa = '';
    // if (objectHewan.nama === 'Miaw') {
    //   hasilSapa = 'Hallo Miaw';
    // } else {
    //   hasilSapa = 'Hewan sapa ni?';
    // }
    // return hasilSapa;
    return objectHewan.nama === 'Miaw' ? 'Halo Miaw' : 'Hewan sokap?';
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>
        Materi Basic Javascript di React Native
      </Text>
      <Text>Nama : {nama}</Text>
      <Text>Usia : {usia}</Text>
      <Text>============</Text>
      <Text>{sapaHwan(hewanPeliharaan)}</Text>
      {namaOrang.map((orang) => (
        <Text>{orang}</Text>
      ))}
    </View>
  );
};

export default BasicJavascript;

const styles = StyleSheet.create({
  container: {padding: 20},
  textTitle: {textAlign: 'center'},
});
