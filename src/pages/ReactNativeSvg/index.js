import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SvgUri} from 'react-native-svg';
import IllustrationMyApp from '../../assets/image/undraw_my_app_grf2.svg';

const ReactNativeSvg = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>
        Materi Menggunakan File SVG didalam React Native
      </Text>
      <IllustrationMyApp width={244} height={125} />
      <SvgUri
        width="100"
        height="100"
        uri="http://thenewcode.com/assets/images/thumbnails/homer-simpson.svg"
      />
    </View>
  );
};

export default ReactNativeSvg;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textTitle: {
    textAlign: 'center',
  },
});
