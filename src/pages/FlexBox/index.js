import React, {Component, useEffect, useState} from 'react';
import {View, Text, Image} from 'react-native';

// class FlexBox extends Component {
//     constructor(props) {
//         super(props);
//         console.log('===> constructor');
//         this.state = {
//             level: 'Software',
//         }
//     }

//     componentDidMount() {
//         console.log('===> component did mount')
//         setTimeout(() => {
//             this.setState({
//                 level: 'Staff'
//             })
//         }, 2000)
//     }

//     componentDidUpdate() {
//         console.log('===> component did update')
//     }

//     componentWillUnmount() {
//         console.log('===> component will amount')
//     }

//     render() {
//         console.log('===> render')
//         return(
//             <View>
//                 <View
//                 style={{
//                     flexDirection: 'row',
//                     backgroundColor: '#C8D6E5',
//                     alignItems: 'center',
//                     justifyContent: 'space-between'
//                     }}>
//                     <View style={{backgroundColor: '#EE5253', width: 50, height: 50}} />
//                     <View style={{backgroundColor: '#FECA57', width: 50, height: 50}} />
//                     <View style={{backgroundColor: '#1DD1A1', width: 50, height: 50}} />
//                     <View style={{backgroundColor: '#5F27CD', width: 50, height: 50}} />
//                 </View>
//                 <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
//                     <Text>Beranda</Text>
//                     <Text>Video</Text>
//                     <Text>Playlist</Text>
//                     <Text>Komunitas</Text>
//                     <Text>Channel</Text>
//                     <Text>Tentang</Text>
//                 </View>
//                 <View style={{flexDirection: 'row', alignItems: "center", marginTop: 20}}>
//                     <Image source={{uri : 'https://portal.lumoshive.com/public/profilepic/837.png'}} style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}} />
//                     <View>
//                         <Text style={{fontSize: 20, fontWeight: 'bold'}}>Rochman Ramadhani Chiefto Irawan</Text>
//                         <Text>{this.state.level} Engineer</Text>
//                     </View>
//                 </View>
//             </View>
//         );
//     }
// }

const FlexBox = () => {
  // handle perubahan dengan useState()
  const [title, setTitle] = useState('Software');
  useEffect(() => {
    console.log('did mount');
    setTimeout(() => {
      setTitle('Staff');
    }, 2000);
    // fungsi pembersih a.k.a clearance
    // return dapat bermakna update dan juga WillAmount
    return () => {
      console.log('did update');
    };
  }, [title]);

  // useEffect(() => {
  //     console.log('did updated')
  //     setTimeout(() => {
  //         setTitle('Staff')
  //     }, 2000)
  // }, [title]);

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#C8D6E5',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View style={{backgroundColor: '#EE5253', width: 50, height: 50}} />
        <View style={{backgroundColor: '#FECA57', width: 50, height: 50}} />
        <View style={{backgroundColor: '#1DD1A1', width: 50, height: 50}} />
        <View style={{backgroundColor: '#5F27CD', width: 50, height: 50}} />
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <Text>Beranda</Text>
        <Text>Video</Text>
        <Text>Playlist</Text>
        <Text>Komunitas</Text>
        <Text>Channel</Text>
        <Text>Tentang</Text>
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
        <Image
          source={{
            uri: 'https://portal.lumoshive.com/public/profilepic/837.png',
          }}
          style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
        />
        <View>
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>
            Rochman Ramadhani Chiefto Irawan
          </Text>
          <Text>{title} Engineer</Text>
        </View>
      </View>
    </View>
  );
};

export default FlexBox;
