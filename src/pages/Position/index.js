import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import google from '../../assets/icon/google.png';

const Position = () => {
  return (
    <View style={styles.wrapper}>
      <Text>Materi Position</Text>
      <View style={styles.googleWrapper}>
        <Image source={google} style={styles.iconGoogle} />
        <Text style={styles.notif}>10</Text>
      </View>
      <Text style={styles.text}>You Work in Here</Text>
    </View>
  );
};

export default Position;

const styles = StyleSheet.create({
  wrapper: {padding: 20, alignItems: 'center'},
  googleWrapper: {
    borderWidth: 1,
    borderColor: '#4398D1',
    width: 93,
    height: 93,
    borderRadius: 93 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    marginTop: 40,
  },
  iconGoogle: {width: 50, height: 50},
  text: {fontSize: 12, color: '#777777', fontWeight: '700', marginTop: 12},
  notif: {
    fontSize: 12,
    color: 'white',
    backgroundColor: '#6FCF97',
    padding: 4,
    borderRadius: 25,
    width: 24,
    height: 24,
    position: 'absolute',
    top: 0,
    right: 0,
  },
});

// kalau flexDirection: 'row', garis horizontalnya ditentukan oleh justifyContent
// kalau flexDirection: 'column' (default), garis horizontalnya ditentukan oleh alignItems
